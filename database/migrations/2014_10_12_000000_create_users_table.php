<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->date('birth_day');
            $table->string('adress');
            $table->string('phone')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('img_file');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert(
           array(
              'first_name'=>'Admin',
              'last_name'=>'Admin',
              'adress'=>'Programming center',
              'birth_day'=>date("Y-m-d"),
              'phone'=>'+998991234567',
              'email' => 'admin@test.uz',
              'created_at'=>date("Y-m-d H:i:s"),
              'password'=>bcrypt('123456')
           )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
