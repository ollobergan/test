@extends('layouts.app')

@section('content')



    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">Профиль
                        <a href="{{ url('profile/edit') }}" class=" pull-right"><i class="fa fa-edit"></i> Редактировать</a>
                    </div>
                    <div class="panel-body">
                        @if (session('edit_status'))
                            <center>
                                <div class="alert alert-success">
                                    {{ session('edit_status') }}
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                </div>
                            </center>
                        @endif
                        <div class="col-xs-8">
                            <div style="padding: 5px;">
                                <b>Имя:</b><br>
                                {{$user->first_name}}<br>
                            </div>
                            <div style="padding: 5px;">
                                <b>Фамилия:</b><br>
                                {{$user->last_name}}<br>
                            </div>
                            <div style="padding: 5px;">
                                <b>День рождения:</b><br>
                                {{$user->birth_day}}<br>
                            </div>
                            <div style="padding: 5px;">
                                <b>Адрес:</b><br>
                                {{$user->adress}}<br>
                            </div>
                            <div style="padding: 5px;">
                                <b>Телефон:</b><br>
                                {{$user->phone}}<br>
                            </div>
                            <div style="padding: 5px;">
                                <b>Адрес электронной почты:</b><br>
                                {{$user->email}}<br>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            @if($user->img_file!="")
                            <img src="{{url('public/uploads/'.$user->img_file)}}" width="90">
                                @else
                                <img src="{{url('public/assets/img/no-avatar.jpg')}}" width="90">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function setupViewJS() {

        }
    </script>
@endsection