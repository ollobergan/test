<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;
use Validator;
use File;

class ProfileController extends Controller
{
    public function __construct()
    {
        //check authentication
        $this->middleware('auth');

    }

    /**
     * Show the profile page
     */
    public function index()
    {
        $user=DB::table("users")->find(Auth::user()->id);
        return view('profile.index',['user'=>$user]);
    }

    /**
     * Show the profile edit form
     */
    public function showedit()
    {
        $user=DB::table("users")->find(Auth::user()->id);
        return view('profile.edit',['user'=>$user]);
    }
    /**
     * Edit user in database
     */
    public function edit(Request $request){
        $user=Auth::user();
        $user_from_db = DB::table("users")->find($user->id);
        $this->validator($request,$user);
        if ($request->hasFile('file')){
            $file = $request->file('file');
            $new_file_name='avatar'.date("Y_m_d_H_i_s").'.'.$file->getClientOriginalExtension();
            $file->move('public/uploads', $new_file_name);
            $request['img_file'] =$new_file_name;
            $file_path = public_path()."/uploads/".$user_from_db->img_file;
            if(file_exists($file_path)){
                File::delete($file_path);
            }
        }

        if ($request->has('password')){
            $request['password']=bcrypt($request->password);
        }
        $user->update(array_filter($request->all()));
        return redirect("/profile")->with("edit_status","Профиль был успешно изменен!");
    }

    /**
     * Validation request
     */
    protected function validator($data,$user)
    {

        $validation_rules=array(
           'first_name' => 'required|max:255',
           'last_name' => 'required|max:255',
           'adress' => 'required',
           'birth_day' => 'required|date',
           'phone' => 'required|unique:users,phone,'.$user->id,
           'email' => 'required|email|max:255|unique:users,email,'.$user->id,
           'password' => 'min:6|confirmed',
           'file'=>'max:10000|mimes:jpg,jpeg,png,bmp',
        );

        return $this->validate($data, $validation_rules, [ ],[
              'phone' => 'Телефон',
              'first_name' => 'имя',
              'last_name' => 'фамилия',
              'birth_day' => 'день рождения',
              'address' => 'адрес',
              'email' => 'эл. почта',
              'password' => 'пароль',
              'file'=>'изображение',
           ]
        ) ;
    }

}