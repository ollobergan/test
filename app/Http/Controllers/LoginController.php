<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Auth;

class LoginController extends Controller
{
    /**
     * Show the registration form
     */
    public function showregisterform(){
        return view("auth.register");
    }

    /**
     * Show the login form
     */
    public function showloginform(){
        return view("auth.login");
    }

    /**
     * The registration user to database
     */
    public function register(Request $request){
        try {
            $this->validator($request);
            if ($request->hasFile('file')){
                $file = $request->file('file');
                $newfilename='avatar'.date("Y_m_d_H_i_s").'.'.$file->getClientOriginalExtension();
                $file->move('public/uploads', $newfilename);
                $request['img_file'] =$newfilename;
            }
            $request['password']=bcrypt($request->password);
            User::create($request->all());
        } catch (FileException $exception) {
            throw $exception;
        }
        return redirect("/login")->with("status","вы зарегистрированы!");
    }

    /**
     * Check the login
     */
    public function login(Request $request){
        $this->validate($request, [
           'email' => 'required|email|max:255',
           'password' => 'required|min:6',
        ],[],[
              'email' => 'эл. почта',
              'password' => 'пароль',
           ]
        );
        if(Auth::attempt(['email'=>$request->email , 'password'=>$request->password])){
            return redirect("/home");
        }
        return redirect("/login")->with("error_message","Неверный логин или пароль");
    }

    /**
     * Validation request
     */
    protected function validator($data)
    {
        return $this->validate($data, [
           'first_name' => 'required|max:255',
           'last_name' => 'required|max:255',
           'adress' => 'required',
           'birth_day' => 'required|date',
           'phone' => 'required|unique:users',
           'email' => 'required|email|max:255|unique:users',
           'password' => 'required|min:6|confirmed',
           'file'=>'max:10000|mimes:jpg,jpeg,png,bmp',

        ], [ ],[
              'first_name' => 'имя',
              'last_name' => 'фамилия',
              'birth_day' => 'день рождения',
              'address' => 'адрес',
              'phone' => 'Телефон',
              'email' => 'эл. почта',
              'password' => 'пароль',
              'file'=>'изображение',
           ]
        ) ;
    }

    /**
     * Logout
     */
    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }
}