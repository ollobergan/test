<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/home', 'HomeController@index');

Route::get('/register', 'LoginController@showregisterform');
Route::post('/register', 'LoginController@register');

Route::get('/login', 'LoginController@showloginform');
Route::post('/login', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');

Route::controllers([
   'auth' => 'Auth\AuthController',
   'password' => 'Auth\PasswordController',
]);

Route::get('/profile', 'ProfileController@index');
Route::get('/profile/edit', 'ProfileController@showedit');
Route::post('/profile/edit', 'ProfileController@edit');
